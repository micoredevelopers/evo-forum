import $ from 'jquery'
const button = $('.main__participation__card-button');

export default function buyModal() {
    
    button.on('click', function(){
        const that = $(this);
        const cardTitle = that.parents('.main__participation__card').find('.main__participation__card-status').text();
        $('.modal__form-title span').text(cardTitle);
    })
}