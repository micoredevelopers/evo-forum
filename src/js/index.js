import 'particles.js'
import burgerMenu from './burger-menu'
import setHeader from './header'
import hoistingButton from './hoisting-button'
import tabs from './tabs'
import buyModal from './modal-buy'
import tabFilter from './tabs-filter'
import addModal from './modal-forms'
import hashtagsFilter from './hashtags-filter';
import onButton from "./partners-cards";

$(window).on('scroll', function () {
  if ($(window).scrollTop() > 5) {
    $('.header__main__container').addClass('active')
  } else {
    $('.header__main__container').removeClass('active')
  }
})


if (window.innerWidth > 1199 && $('main').hasClass('main')) {
  particlesJS.load('particles-js', 'particles.json');
}

addModal()
setHeader()
burgerMenu()
hoistingButton()
tabs()
buyModal()
tabFilter()
hashtagsFilter();
onButton();


$(document).ready(function(){
  // $("[name=phone]").mask("+380(99) 999-9999");
  $('.main__support__slick-slider').slick({
    arrow: true ,
    prevArrow: '<button id="prev" type="button" class="slick__prev"><img class="slider__prev-img" src="images/slider-arrow.png"></button>',
    nextArrow: '<button id="next" type="button" class="slick__next"><img class="slider__next-img" src="images/slider-arrow.png"></button>'
  });

  $('.main__support__slick-slider').on('afterChange', function(event, slick, direction){
    $('.main__support__content-card_description').removeClass('d-block');
  });
});

