import $ from 'jquery';

export default function onButton () {
    $('.main__support__expand_description').on('click', function () {
        $(this).toggleClass('expand__active');
        $(this).parent().parent().children('.main__support__content-card_description').slideToggle();
    })
}
