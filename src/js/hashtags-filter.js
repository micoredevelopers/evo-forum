import $ from 'jquery';

export default function hashtagsFilter () {
  $('.main__hashtags span').on('click', function () {
    const spanId = $(this).attr('id');
    $(`[data-info]`).addClass('d-none');
    $(`[data-info='${spanId}']`).removeClass('d-none');
    
  })
}