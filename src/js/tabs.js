import $ from 'jquery';

const main = $('main')

export default function tabs () {
    main.hasClass('main') && $('.main__support__content').append(` <div class="main__support-buttons">
    <a  id="supportAll" class="main__support-button main__support-button_active">Все</a>
    <a  id="supportPartners" class="main__support-button">Партнёры</a>
    <a  id="supportInformative" class="main__support-button">Информационные</a>
</div>
<div class="main__support-cards ${main.hasClass('main') ? 'main-tabs' : ''}">
    <div class="main__support-card" data-filter="supportPartners"></div>
    <div class="main__support-card" data-filter="supportPartners"></div>
    <div class="main__support-card" data-filter="supportInformative"></div>
    <div class="main__support-card" data-filter="supportInformative"></div>
    <div class="main__support-card" data-filter="supportPartners"></div>
    <div class="main__support-card" data-filter="supportPartners"></div>
</div>`)
}