import $ from 'jquery';

export default function addModal() {
    $('body').append(`<div class="modal fade" id="modalBuy" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>

        <form class="modalCallbackForm" name="callback">
            <div class="modal__form-title with-beauty-line"><span class="with-beauty-line2">Название пакета</span></div>
            <div class="modal__form-inputs">
                <input type="text" class="modal__form-input" name="fullname" placeholder="Имя, Фамилия">
                <input type="email" class="modal__form-input" name="email" placeholder="Почта">
                <input type="text" class="modal__form-input" name="phone" placeholder="Телефон">
            </div>
            <div class="modal__form-agreement">
                <label class="agreement__checkbox">
                  <input type="checkbox" class="agreement__real-checkbox">
                  <svg class="agreement__fake-checkbox_svg"  fill="url(#gradient1) #36f2e3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 349.68 263.95"><defs>
        <linearGradient id="gradient1" >
          <stop offset="0%" stop-color="#36f2e3" />
          <stop offset="100%" stop-color="#ba89f4" />
        </linearGradient>
      </defs><g class="svg-color" data-name="Слой 2"><g id="Слой_1-2" data-name="Слой 1"><path d="M349.68,26.69,319.88,0l-185,206.57-107.35-102L0,133.51,137.22,264Z"/></g></g></svg>
                </label>
                <div class="agreement-text">«Ознакомлен(а) с правилами, политикой конфиденциальности и даю согласие на получение предложений от партнёров мероприятия!»</div>
            </div>
            <button type="submit" class="modal__form-button">Оплатить</button>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalCallback" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  
    <div class="modal-dialog">
      <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
            </button>

        <form class="modalCallbackForm" name="callback">
            <div class="modal__form-title">Обратная связь</div>
            <div class="modal__form-inputs">
                <input type="text" class="modal__form-input" name="fullname" placeholder="Имя, Фамилия">
                <input type="text" class="modal__form-input" name="phone" placeholder="Телефон">
            </div>
            <div class="modal__form-agreement">
                <label class="agreement__checkbox">
                  <input type="checkbox" class="agreement__real-checkbox">
                  <svg class="agreement__fake-checkbox_svg" fill="url(#gradient2) #36f2e3"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 349.68 263.95"> <defs>
                    <linearGradient id="gradient2">
                      <stop offset="0%" stop-color="#36f2e3"/>
                      <stop offset="100%" stop-color="#ba89f4" />
                    </linearGradient>
                  </defs><g class="svg-color" data-name="Слой 2"><g id="Слой_1-2" data-name="Слой 1"><path d="M349.68,26.69,319.88,0l-185,206.57-107.35-102L0,133.51,137.22,264Z"/></g></g></svg>
                </label>
                <div class="agreement-text">«Ознакомлен(а) с правилами, политикой конфиденциальности и даю согласие на получение предложений от партнёров мероприятия!»</div>
            </div>
            <button type="submit" class="modal__form-button">Отправить</button>
        </form>
           
      </div>
    </div>
  </div>
`)
}