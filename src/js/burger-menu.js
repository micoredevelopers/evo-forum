
import scroll from 'page-scroll-to-id';

export default function burgerMenu() {

    $('.header__burger-menu').on('click', () => {
        $('.header__nav-wrapper').addClass('show__menu');
        $('body').addClass('body_active');
    })

    $('.header__nav-close').on('click', () => {
        $('.header__nav-wrapper').removeClass('show__menu');
        $('body').removeClass('body_active');
    })


    $('.header__nav-item').on('click', ()=> {
        $('.header__nav-wrapper').removeClass('show__menu');
        $('body').removeClass('body_active');
    })


    $('a').on('click', ()=>{
        $('a').mPageScroll2id();
    })



}
